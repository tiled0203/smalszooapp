import {BrowserModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {HomeComponent} from './components/home/home.component';
import {AnimalService} from './services/animal.service';
import {RouterRoutingModule} from './router/router-routing.module';
import {AnimalListComponent} from './components/animal-list/animal-list.component';
import {AddAnimalComponent} from './components/add-animal/add-animal.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AnimalDetailComponent} from './components/animal-detail/animal-detail.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatOptionModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import {LoginComponent} from './components/login/login.component';
import {AdminComponent} from './components/admin/admin.component';
import {HttpInterceptor} from './interceptors/HttpInterceptor';
import {ChatClientComponent} from './components/chat-client/chat-client.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {faLocationArrow} from '@fortawesome/free-solid-svg-icons';


@NgModule({
  declarations: [
    HomeComponent,
    AnimalListComponent,
    AddAnimalComponent,
    AnimalDetailComponent,
    LoginComponent,
    AdminComponent,
    ChatClientComponent],
  imports: [
    BrowserModule,
    RouterRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatButtonModule,
    MatOptionModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    ReactiveFormsModule,
    NgbModule,
    FontAwesomeModule
  ],
  providers: [AnimalService, {provide: HTTP_INTERCEPTORS, useClass: HttpInterceptor, multi: true}],
  bootstrap: [HomeComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faLocationArrow);
  }
}
