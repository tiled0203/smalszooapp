import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Credentials} from '../components/model/credentials';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {User} from "../components/model/user";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authenticated = false;
  headers: HttpHeaders = new HttpHeaders();
  user: User;

  constructor(private http: HttpClient, private router: Router) {
  }

  authenticate(credentials: Credentials): Promise<void> {
    console.log(credentials.userEmail);
    this.headers = new HttpHeaders(credentials ? {
      authorization: 'Basic ' + btoa(credentials.userEmail + ':' + credentials.password)
    } : {});

    return this.http.get<User>('/users', {headers: this.headers}).pipe(map(responseUserObject => {
      if (responseUserObject) {
        this.user = responseUserObject;
        this.authenticated = true;
      } else {
        this.authenticated = false;
      }
    })).toPromise();
  }

  logout(): void {
    this.http.get<string>('/logout').toPromise().then(() => {
      this.authenticated = false;
      this.user = undefined;
      this.headers = new HttpHeaders();
      this.router.navigate(['/login']);
    });
  }
}
