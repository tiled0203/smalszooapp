import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Message} from '../components/model/message';
import {CompatClient, Stomp, StompSubscription} from '@stomp/stompjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  // tslint:disable-next-line:variable-name
  private _messages: Subject<Message> = new Subject();
  messages$: Observable<Message> = this._messages.asObservable();
  private stompClient: CompatClient;
  private subs: StompSubscription;
  private reTryCount = 0;

  constructor() {
  }

  connect(): void {
    if (this.stompClient === undefined) {
      this.create(environment.webSocketUrl);
    }
  }

  private create(url): void {
    this.stompClient = Stomp.over(() => {
      return new WebSocket(url);
    });
    this.stompClient.connect({}, () => {
      this.subs = this.stompClient.subscribe('/topic/responses', msg => {
        this._messages.next(JSON.parse(msg.body));
      });
    });
  }

  send(msg: Message): void {
    if (this.stompClient !== undefined && this.stompClient.connected) {
      this.reTryCount = 0;
      this.stompClient.send('/app/request', null, JSON.stringify(msg));
    } else if (this.reTryCount < 5) {
      this.stompClient = undefined;
      this.connect();
      this.reTryCount += 1;
      setTimeout(() => {
        this.send(msg);
      }, 2000);
    } else {
      console.error('Cannot connect to websocket');
      this.reTryCount = 0;
    }
  }

  disconnect(): void {
    if (this.stompClient !== undefined) {
      this.stompClient.disconnect();
      this.subs.unsubscribe();
      this.stompClient = undefined;
    }
  }
}
