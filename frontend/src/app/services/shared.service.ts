import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {AddAnimalComponent} from '../components/add-animal/add-animal.component';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private messageSubject: Subject<any> = new Subject<any>();
  messageObserver: Observable<string> = this.messageSubject.asObservable();

  constructor() {
  }

  send(clazz, msg: string): void {
      this.messageSubject.next(msg);
  }
}
