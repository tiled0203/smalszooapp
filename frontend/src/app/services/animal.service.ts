import {Injectable} from '@angular/core';
import {Animal} from '../components/model/animal';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {Page} from '../components/model/page';
import {map} from 'rxjs/operators';
import {Pageable} from '../components/model/pageable';


@Injectable({
  providedIn: 'root'
})
export class AnimalService {
  animals$: BehaviorSubject<Array<Animal>> = new BehaviorSubject<Array<Animal>>([]);
  pageable$: BehaviorSubject<Pageable> = new BehaviorSubject<Pageable>({pageNumber: 0, pageSize: 20});
  total$: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  constructor(private http: HttpClient) {
    this.initAnimalObservable();
  }

  private initAnimalObservable(): void {
    let httpParams = new HttpParams();
    this.pageable$.subscribe(page => {
      httpParams = httpParams.set('page', (page.pageNumber - 1).toString());
      httpParams = httpParams.set('size', page.pageSize.toString());
      this.http.get<Page<Animal>>(`/animals/pageable`, {params: httpParams})
        .pipe(map(value => {
          this.total$.next(value.totalElements);
          return value.content;
        }))
        .subscribe(animals => this.animals$.next(animals));
    });
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`/animals/${id}`);
  }

  findById(id: any): Observable<Animal> {
    return this.http.get<Animal>(`/animals/${id}`);
  }

  save(animal: Animal, imageFile: any): Observable<any> {
    const formData = new FormData();
    const json = JSON.stringify(animal);
    console.log(json);
    const blob = new Blob([json], {
      type: 'application/json'
    });
    formData.append('file', imageFile);
    formData.append('animal', blob);
    return this.http.post('/animals', formData);
  }

  getAnimalTypes(): Observable<string[]> {
    return this.http.get<string[]>('/animals/animalTypes');
  }

  set pageable(pageable: Pageable) {
    this.pageable$.next(pageable);
  }
}
