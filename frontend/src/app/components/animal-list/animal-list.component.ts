import {Component, OnInit} from '@angular/core';
import {Animal} from '../model/animal';
import {AnimalService} from '../../services/animal.service';
import {Observable, Subscription} from 'rxjs';
import {SharedService} from '../../services/shared.service';
import {AutoUnsubscribe} from '../../decorator/AutoUnsubscribe';
import {AuthService} from '../../services/auth.service';
import {Pageable} from '../model/pageable';

@Component({
  selector: 'app-animal-list',
  templateUrl: './animal-list.component.html',
  styleUrls: ['./animal-list.component.css']
})
@AutoUnsubscribe
export class AnimalListComponent implements OnInit {

  private sharedServiceSub: Subscription;
  animals: Observable<Array<Animal>>;
  // tslint:disable-next-line:variable-name
  pageable: Pageable = new Pageable(1, 20);
  total: Observable<number>;

  constructor(private animalService: AnimalService, private sharedService: SharedService, public authService: AuthService) {
    this.animals = animalService.animals$;
    this.total = animalService.total$;
  }

  ngOnInit(): void {
    this.sharedServiceSub = this.sharedService.messageObserver.subscribe(value => console.log('AnimalListComponent ' + value));
  }

  delete(id: number): void {
    this.animalService.delete(id).subscribe(value => {
      this.animalService.pageable$.next({pageSize: this.pageable.pageSize, pageNumber: this.pageable.pageNumber});
    });
  }

  getPage(): void {
    console.log(this.pageable);
    this.animalService.pageable = this.pageable;
  }


}
