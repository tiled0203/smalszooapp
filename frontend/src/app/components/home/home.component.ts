import {Component, OnInit} from '@angular/core';
import {Animal} from '../model/animal';
import {AnimalService} from '../../services/animal.service';
import {ZooGuard} from '../../services/zoo-guard.service';
import {AuthService} from '../../services/auth.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  title = 'Welcome to the zoo!';


  constructor(public auth: AuthService) {
  }

  logout(): void {
    this.auth.logout();
  }
}
