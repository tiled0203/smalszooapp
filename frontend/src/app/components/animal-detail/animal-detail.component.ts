import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {SharedService} from '../../services/shared.service';
import {ActivatedRoute, UrlSegment} from '@angular/router';
import {AnimalService} from '../../services/animal.service';
import {Animal} from '../model/animal';
import {Subscription} from 'rxjs';
import {AutoUnsubscribe} from '../../decorator/AutoUnsubscribe';

@Component({
  selector: 'app-animal-detail',
  templateUrl: './animal-detail.component.html',
  styleUrls: ['./animal-detail.component.css']
})

export class AnimalDetailComponent implements OnInit {
  constructor(private route: ActivatedRoute, private sharedService: SharedService, private animalService: AnimalService) {
  }

  private static subscription: Subscription;
  animal: Animal;
  @Input() id: string;
  @Output() deleteEvent = new EventEmitter();
  isDetail = false;

  ngOnInit(): void {
    this.isDetail = this.route.snapshot.url.find(value => value).path !== 'detail';
    if (AnimalDetailComponent.subscription === undefined) {
      AnimalDetailComponent.subscription = this.sharedService.messageObserver
        .subscribe(value => console.log('AnimalDetailComponent ' + value));
    }
    if (this.id !== undefined) {
      this.animalService.findById(this.id).subscribe(animal => this.animal = animal);
    } else {
      this.animal = this.route.snapshot.data.animal;
    }
  }

  delete(id): void {
    this.deleteEvent.emit(id);
  }
}
