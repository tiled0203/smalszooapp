import {Component, OnDestroy, OnInit} from '@angular/core';
import {ChatService} from '../../services/chat.service';
import {Message} from '../model/message';
import {Subscription} from 'rxjs';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-chat-client',
  templateUrl: './chat-client.component.html',
  styleUrls: ['./chat-client.component.css']
})
export class ChatClientComponent implements OnInit, OnDestroy {
  message: Message = {author: undefined, message: undefined, timeDate: undefined};

  messages: Message[] = [];
  private subscription: Subscription;
  chatWith: string;

  constructor(private chatService: ChatService, public authService: AuthService) {

  }

  ngOnInit(): void {
    this.chatService.connect();
    this.subscription = this.chatService.messages$.subscribe(msg => {
      if ((msg.author !== this.authService.user.userName) && this.chatWith === undefined) {
        this.chatWith = msg.author;
      }
      this.messages.push(msg);
    });
  }

  sendMsg(): void {
    this.message.author = this.authService.user.userName;
    console.log('new message from client to websocket: ', this.message);
    this.chatService.send(this.message);
    this.message.message = '';
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


}
