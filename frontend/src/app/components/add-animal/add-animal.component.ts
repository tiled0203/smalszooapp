import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Navigation, Router} from '@angular/router';
import {SharedService} from '../../services/shared.service';
import {Animal} from '../model/animal';
import {AnimalService} from '../../services/animal.service';
import {AutoUnsubscribe} from '../../decorator/AutoUnsubscribe';
import {Observable, of, Subject, Subscription} from 'rxjs';
import {FormArray, FormBuilder, FormControl, FormGroup, NgForm, ValidationErrors} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {catchError, debounceTime, map} from 'rxjs/operators';
import {ForbiddenNameValidator} from '../../validators/ForbiddenNameValidator';


@Component({
  selector: 'app-add-animal',
  templateUrl: './add-animal.component.html',
  styleUrls: ['./add-animal.component.css']
})
@AutoUnsubscribe
export class AddAnimalComponent implements OnInit {
  @ViewChild('fileUpload', {static: false}) fileUpload: ElementRef;
  files = [];

  animalTypes: string[] = [];
  private animalServiceSub: Subscription;
  showDetails = false;
  animalId: any;
  private imageFile: any;

  animalFrom: FormGroup;
  formTemplate: any = [{type: 'number', label: 'age', value: 70}
    , {label: 'box1', type: 'textBox', value: 'hello!'}
    , {label: 'select1', type: 'select', options: ['a', 'b']}
  ];


  constructor(private formBuilder: FormBuilder, private router: Router
    ,         private sharedService: SharedService, private animalService: AnimalService) {
  }

  ngOnInit(): void {
    this.animalFrom = this.formBuilder.group({
      name: ['', ForbiddenNameValidator(new RegExp('test'))],
      animalType: ['']
    });

    this.formTemplate.forEach(data => {
      this.animalFrom.addControl(data.label, new FormControl(data.value));
    });

    this.sharedService.send(this, 'hello');
    this.animalServiceSub = this.animalService.getAnimalTypes().subscribe(reponse => {
      this.animalTypes = reponse;
    });

  }


  save(): void {
    if (this.animalFrom.valid) {
      this.animalService.save(this.animalFrom.value, this.imageFile).subscribe((response: Animal) => {
        console.log('animal created with id:' + response.id);
        this.animalId = response.id;
        this.showDetails = true;
      }, error => {
        console.error(error);
      });
    }
  }

  deleteAnimal(id: any): void {
    console.log('delete animal with id: ' + id);
    this.animalService.delete(id).subscribe(value => {
      this.router.navigate(['']);
    });
  }


  onClick(): void {
    const fileUpload = this.fileUpload.nativeElement;
    fileUpload.onchange = () => {
      this.imageFile = fileUpload.files[0];
    };
    fileUpload.click();
  }

}
