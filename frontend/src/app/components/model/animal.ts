import {Food} from './food';

export class Animal {
  id;
  name;
  food: Food;
  foodType: string;
  animalType: string;
  image?: object;
  description?: string;

  constructor(name?) {
    this.name = name;
  }
}

