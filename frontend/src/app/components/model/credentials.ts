export interface Credentials {
  userEmail: string;
  password: string;
}
