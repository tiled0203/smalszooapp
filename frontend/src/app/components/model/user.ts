export interface User {
  userEmail: string;
  userName: string;
}
