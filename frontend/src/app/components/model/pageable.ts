export class Pageable {
  constructor(pageNumber: number, pageSize: number) {
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
  }

  pageNumber;
  pageSize;
}
