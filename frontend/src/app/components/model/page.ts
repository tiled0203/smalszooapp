export interface Page<T> {
  totalElements: number; // total items
  totalPages: number;      // total number of pages
  numberOfElements: number; // total number of items per Page
  number: string;       // current page
  content: Array<T>;  // items for the current page
}
