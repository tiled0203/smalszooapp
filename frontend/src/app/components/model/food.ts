export interface Food {
  id: number;
  foodType: string;
  foodName: string;
  animalType: string;
}
