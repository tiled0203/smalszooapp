export interface Message {
  author: string;
  message: string;
  timeDate: Date;
}
