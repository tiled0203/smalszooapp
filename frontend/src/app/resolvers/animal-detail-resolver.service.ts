import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AnimalService} from '../services/animal.service';
import {Animal} from '../components/model/animal';

@Injectable({
  providedIn: 'root'
})
export class AnimalDetailResolverService implements Resolve<Animal> {

  constructor(private animalService: AnimalService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Animal> {
    return this.animalService.findById(route.params.id);
  }
}
