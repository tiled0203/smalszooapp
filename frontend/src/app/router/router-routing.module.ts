import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AnimalListComponent} from '../components/animal-list/animal-list.component';
import {AddAnimalComponent} from '../components/add-animal/add-animal.component';
import {AnimalDetailComponent} from '../components/animal-detail/animal-detail.component';
import {AdminComponent} from '../components/admin/admin.component';
import {ZooGuard} from '../services/zoo-guard.service';
import {LoginComponent} from '../components/login/login.component';
import {AnimalDetailResolverService} from '../resolvers/animal-detail-resolver.service';
import {ChatClientComponent} from '../components/chat-client/chat-client.component';

const routes: Routes = [
  {path: '', component: AnimalListComponent},
  {path: 'add', component: AddAnimalComponent, canActivate: [ZooGuard]},
  {
    path: 'detail/:id', component: AnimalDetailComponent, resolve: {
      animal: AnimalDetailResolverService
    }
  },
  {path: 'admin', component: AdminComponent, canActivate: [ZooGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'chat', component: ChatClientComponent, canActivate: [ZooGuard]},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class RouterRoutingModule {
}
