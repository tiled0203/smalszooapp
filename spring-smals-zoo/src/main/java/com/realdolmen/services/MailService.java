package com.realdolmen.services;

import javax.mail.MessagingException;

public interface MailService {
    void sendSimpleMessage(String to, String subject, String text) throws MessagingException;
}
