package com.realdolmen.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class MailServiceImpl implements MailService {
    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private SimpleMailMessage template;
    
    @Override
    public void sendSimpleMessage(String to, String subject, String text) throws MessagingException {
        MimeMessage mimeMessage = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        ClassPathResource image = new ClassPathResource("/ascii-art.txt");
        helper.addAttachment("ascii-art.txt",image);
        helper.setFrom("noreply@realdolmen.com");
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(String.format(template.getText(), text), true);
        emailSender.send(mimeMessage);
    }
}
