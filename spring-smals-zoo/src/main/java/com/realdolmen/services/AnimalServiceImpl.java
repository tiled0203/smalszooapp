package com.realdolmen.services;


import com.realdolmen.domain.Animal;
import com.realdolmen.repositories.springdata.AnimalRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Qualifier("animalService")
@Transactional
public class AnimalServiceImpl implements AnimalService { // renamed MyService to GenericCrudService

    //    @Autowired // old way of dependency injection (not recommended)
    private AnimalRepository animalRepository;

    public AnimalServiceImpl(AnimalRepository animalRepository) {
        this.animalRepository = animalRepository;
    }

    @Override
    @Cacheable(value = "animalCache")
    public List<Animal> findAll() {
        return animalRepository.findAll();
    }

//    @Cacheable(value = "animalCache")
    public Page<Animal> findAllWithPaging(Pageable page) {
        return animalRepository.findAll(page);
    }

    @Override
    @CachePut(value = "animalCache")
    public Animal save(Animal animal) {
        return animalRepository.save(animal);
    }

    @Override
    @CacheEvict(value = "animalCache")
    public int deleteById(int id) throws NotFoundException {
        findById(id);
        animalRepository.deleteById(id);
        return id;
    }


    @Override
    public List<Animal> findByName(String name) {
        return animalRepository.findByName(name);
    }

    @Override
    public int count() {
        return findAll().size();
    }

    @Override
    public Animal findById(int id) throws NotFoundException {
        return animalRepository.findById(id).orElseThrow(() -> new NotFoundException("Animal with id " + id + " not found"));
    }

}
