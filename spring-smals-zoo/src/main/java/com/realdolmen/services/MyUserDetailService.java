package com.realdolmen.services;

import com.realdolmen.domain.AppUser;
import com.realdolmen.repositories.springdata.UserRepository;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Getter
    private AppUser user;

    @Override
    public UserDetails loadUserByUsername(String email) {
        this.user = userRepository.findAppUserByUserEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("Not found!");
        }

        return User.withUsername(user.getUserEmail())
                .password(user.getPassword())
                .authorities(user.getRoles()).build();
    }
}
