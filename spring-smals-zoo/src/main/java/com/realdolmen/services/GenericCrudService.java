package com.realdolmen.services;

import com.realdolmen.domain.Animal;
import javassist.NotFoundException;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

public interface GenericCrudService<T> {  // renamed myService to GenericCrudService
    List<T> findAll();

    void save(T entity);

    void deleteById(int id);

    T findByName(String name);

    int count();

    default Animal findById(int id) throws NotFoundException {
        throw new NotImplementedException();
    }
}
