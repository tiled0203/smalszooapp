package com.realdolmen.services;

import com.realdolmen.domain.Animal;
import javassist.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AnimalService {
    List<Animal> findAll();

    Page<Animal> findAllWithPaging(Pageable page);

    Animal save(Animal animal);

    int deleteById(int id) throws NotFoundException;

    List<Animal> findByName(String name);

    int count();

    Animal findById(int id) throws NotFoundException;
}
