package com.realdolmen.services;

import com.realdolmen.domain.Animal;
import com.realdolmen.domain.Food;
import com.realdolmen.domain.enums.AnimalType;
import com.realdolmen.domain.enums.FoodType;
import javassist.NotFoundException;

import java.util.List;

public interface FoodDistributionService {
    List<Food> findFoodByAnimalType(AnimalType animalType);
    void feedAnimals(List<Animal> animals);

    List<Food> findAllFoods();

    Food findFoodById(int id) throws NotFoundException;

    List<FoodType> findAllAvailableFoodTypesByAnimalType(AnimalType animalType);

    List<FoodType> findAllAvailableFoodTypes();
}

