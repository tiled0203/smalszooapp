package com.realdolmen.services;

import com.realdolmen.domain.Visitor;
import com.realdolmen.domain.VisitorPersonalInfoId;
import com.realdolmen.domain.enums.VisitorType;
import com.realdolmen.exception.InvalidActionException;
import com.realdolmen.repositories.springdata.VisitorRepository;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackOn = InvalidActionException.class)
public class VisitorService {

    private VisitorRepository visitorRepository; // using spring data

    public VisitorService(VisitorRepository visitorRepository) {
        this.visitorRepository = visitorRepository;
    }

//    @PostFilter("(hasRole('ROLE_USER') && filterObject.visitorPersonalInfoId.email == principal.username) || hasRole('ROLE_ADMIN')")
    public List<Visitor> findAll() {
        return visitorRepository.findAll();
    }

    public Visitor findByVisitorNameAgeId(VisitorPersonalInfoId visitorPersonalInfoId) throws NotFoundException {
        Optional<Visitor> visitor = visitorRepository.findVisitorsByVisitorPersonalInfoId(visitorPersonalInfoId);
        return visitor.orElseThrow(() -> new NotFoundException("visitor not found"));
    }

    public void save(Visitor visitor) {
        visitorRepository.save(visitor);// spring data
    }

    public List<Visitor> findVisitorsByType(VisitorType visitorType) {
        return visitorRepository.findVisitorsByVisitorType(visitorType);// spring data
    }

    public List<String> findDistinctTypes() {
        return visitorRepository.findDistinctVisitorTypes().stream().map(Enum::name).collect(Collectors.toList());// spring data
    }

    public int countVisitors() {
        return (int) visitorRepository.count(); // spring data
    }

    public void delete(Visitor visitor) throws InvalidActionException {
        visitorRepository.delete(visitor);
//        if(visitor.getId() == 1){
//            throw new InvalidActionException("cannot deleteById number 1 <= this is prohibited");
//        }
    }
}
