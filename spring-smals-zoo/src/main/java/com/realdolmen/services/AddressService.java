package com.realdolmen.services;

import com.realdolmen.domain.Address;
import com.realdolmen.repositories.springdata.AddressRepository;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class AddressService {
    private AddressRepository addressRepository;

    public AddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public Address findById(int id) throws NotFoundException {
        Optional<Address> address = addressRepository.findById(id);
        return address.orElseThrow(() -> new NotFoundException("address not found"));
    }

    public void save(Address address) {
        addressRepository.save(address);
    }
}
