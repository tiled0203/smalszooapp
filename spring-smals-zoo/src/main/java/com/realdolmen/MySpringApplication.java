package com.realdolmen;

import com.realdolmen.services.MailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Scheduled;

import javax.mail.MessagingException;
import java.time.LocalDateTime;

@SpringBootApplication // Spring boot powerful annotation
@Slf4j
public class MySpringApplication implements CommandLineRunner {

    @Qualifier("taskScheduler")
    @Autowired
    private TaskScheduler scheduler;

    @Qualifier("taskExecutor")
    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    private MailService mailService;

    @Autowired
    private JmsTemplate jmsTemplate;

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder(MySpringApplication.class).profiles("dev").build().run();
//        ConfigurableApplicationContext context = SpringApplication.run(MySpringApplication.class);
//        System.out.println(((Bear) animalMyService.findEntityByName("Baloe")).getDateOfBirth());

    }

    @Scheduled(cron = "${mail.cron}")
    private void testSchedule() {
        try {
            log.info("Mail scheduler triggered! " + LocalDateTime.now());
            mailService.sendSimpleMessage("to@example.com", "Timed mail", "Hello this is a test mail " + LocalDateTime.now());
        } catch (MessagingException e) {
            log.error(e.getMessage(), e);
        }
    }


    @Override
    public void run(String... args) throws Exception {
//        scheduler.scheduleAtFixedRate(() -> {
//            System.out.println("schedule:" +LocalDateTime.now());
//        },2000);


//        scheduler.schedule(() -> {
//            System.out.println("schedule:" +LocalDateTime.now());
//        }, new CronTrigger("0/10 * * * * *"));

//        taskExecutor.execute(() -> {
//            System.out.println("execute:" + LocalDateTime.now());
//        });
        jmsTemplate.convertAndSend("helloBox.queue", "Hello, World!");
    }


//
//    @Override
//    public void run(String... args) {
//        executions.printZooProperties();
//        executions.findAllAnimals();
//        executions.feedAllAnimals();
//        executions.findAllBears();
//        executions.saveAnimal();
//        executions.findAnimalByName();
//        executions.findAllVisitors();
//        executions.saveVisitor();
//        executions.findVisitorsByVisitorType();
//        executions.findDistinctvisitortypes();
//        executions.countVisitors();
//        executions.deleteVisitor();
////        try {
////            executions.throwException();
////        } catch (Exception e) {
////        }
//    }
//


}
