package com.realdolmen.controller;

import com.realdolmen.controller.messages.RequestMessage;
import com.realdolmen.controller.messages.ResponseMessage;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class MessageController {
    @MessageMapping("/request") //localhost:8080/app/request (clients send to this url)
    @SendTo("/topic/responses") //localhost:8080/topic/responses (clients subscribe on this url)
    public ResponseMessage send(RequestMessage message) {
        System.out.println(message.getMessage());
        String timeDate = new SimpleDateFormat("HH:mm dd/MM/yyyy").format(new Date());
        return new ResponseMessage(message.getAuthor(), message.getMessage(), timeDate);
    }

    @JmsListener(destination = "helloBox.queue", containerFactory = "container")
    @SendTo("welcome.topic")
    public String receiveHelloMessage(String requestMessage) {
        return requestMessage;
    }


    @JmsListener(destination = "welcome.topic", containerFactory = "container")
    public void receiveWelcomeMessage(String requestMessage) {
        System.out.println("Received <" + requestMessage + ">");
    }

}
