package com.realdolmen.aspects;

import com.realdolmen.exception.InvalidActionException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class Seperator {

    @Pointcut("execution(* com.realdolmen.Executions.*(..))")
    public void addSeperator() {

    }

    @Around("addSeperator()")
    public void printSeperator(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("---------" + joinPoint.getSignature() + "---------");
        if(joinPoint.getSignature().getName().equals("printZooProperties")){
            System.err.println("Not allowed!!");
        }else{
            joinPoint.proceed();
        }
        System.out.println("--------------------------------------------------");
    }

}
