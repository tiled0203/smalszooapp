package com.realdolmen.aspects;

import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class GlobalExceptionHandler {

    @Pointcut("execution(* com.realdolmen.*.*.*(..))")
    public void exceptionPointCut(){}


    @AfterThrowing(value = "exceptionPointCut()" , throwing = "myexception")
    public void printExeption(Exception myexception){
        System.out.println("Error: " + myexception.getMessage());
    }
}
