package com.realdolmen.restcontrollers;

import com.realdolmen.domain.Visitor;
import com.realdolmen.services.VisitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/visitor")
public class VisitorRestController {

    @Autowired
    private VisitorService visitorService;

    @GetMapping("")
    public List<Visitor> getAllVisitors() {
        return visitorService.findAll();
    }

    @GetMapping("/search")
    public List<Visitor> searchVisitors(){
        return visitorService.findAll();
    }

}
