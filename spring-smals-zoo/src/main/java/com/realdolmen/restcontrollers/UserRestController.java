package com.realdolmen.restcontrollers;

import com.realdolmen.domain.AppUser;
import com.realdolmen.domain.Authority;
import com.realdolmen.services.MyUserDetailService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserRestController {
    @Autowired
    private MyUserDetailService userDetailsService;

    @GetMapping("/users")
    public PrincipalDto user(Authentication authentication) {
        if (authentication != null) {
            return new PrincipalDto(userDetailsService.getUser());
        } else {
            return null;
        }
    }
}

@Getter
class PrincipalDto {
    private final String userName;
    private final String userEmail;
    private final List<Authority> authorities;

    public PrincipalDto(AppUser appUser) {
        this.userEmail = appUser.getUserEmail();
        this.authorities = appUser.getRoles();
        this.userName = appUser.getUserName();
    }
}

