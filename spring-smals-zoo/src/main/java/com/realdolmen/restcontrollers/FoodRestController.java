package com.realdolmen.restcontrollers;

import com.realdolmen.domain.Food;
import com.realdolmen.domain.enums.AnimalType;
import com.realdolmen.domain.enums.FoodType;
import com.realdolmen.services.FoodDistributionService;
import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping(path = "/foods")
public class FoodRestController {

    private FoodDistributionService foodDistributionService;

    @GetMapping("")//ex: http:localhost:8080/foods?animalType=BEAR
    public List<Food> searchAllFoods(@RequestParam(value = "animalType", required = false) AnimalType animalType) {
        if (animalType != null) {
            log.info("fetching all foods by animal type {} ", animalType);
            return foodDistributionService.findFoodByAnimalType(animalType);
        } else {
            log.info("fetching all foods");
            return foodDistributionService.findAllFoods();
        }
    }

    @GetMapping("/{id}")
    public Food findById(@PathVariable("id") int id) throws NotFoundException {
        log.info("find food by id {} ", id);
        return foodDistributionService.findFoodById(id);
    }

    @GetMapping("/foodTypes")//ex: http:localhost:8080/foods/foodTypes?animalType=BEAR
    public List<FoodType> findAllAvailableFoodTypes(@RequestParam(value = "animalType", required = false) AnimalType animalType) {
        if (animalType != null) {
            log.info("fetching all food types by animal type {} ", animalType);
            return foodDistributionService.findAllAvailableFoodTypesByAnimalType(animalType);
        } else {
            log.info("fetching all available foodtypes ");
            return foodDistributionService.findAllAvailableFoodTypes();
        }
    }


}
