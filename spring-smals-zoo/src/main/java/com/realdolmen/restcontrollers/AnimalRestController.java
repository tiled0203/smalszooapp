package com.realdolmen.restcontrollers;

import com.realdolmen.domain.Animal;
import com.realdolmen.domain.enums.AnimalType;
import com.realdolmen.dto.AnimalDto;
import com.realdolmen.services.AnimalService;
import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(path = "/animals")
@Slf4j
@AllArgsConstructor
public class AnimalRestController {

    private AnimalService animalService;

    //    @RequestMapping(path = "/animals", method = RequestMethod.GET)
    @GetMapping(path = "")
    public List<Animal> findAllAnimals() {
        log.info("Fetching all animals");
        return animalService.findAll();
    }

    @GetMapping(path = "/pageable")
    public Page<Animal> findAllAnimalsWithPagiging(Pageable pageable) {
        log.info("Fetching animal page {}", pageable.getPageNumber());
        return animalService.findAllWithPaging(pageable);
    }

    @GetMapping(path = "/{id}")
    public Animal findById(@PathVariable("id") int id) throws NotFoundException {
        return animalService.findById(id);
    }

    @PostMapping(path = "", consumes = {"multipart/form-data", "application/json"})
    public Animal saveAnimalImage(@RequestPart(value = "file", required = false) MultipartFile file, @RequestPart("animal") AnimalDto animalDto) throws IOException {
        Animal animal = new Animal();
        animal.setName(animalDto.getName());
        animal.setAnimalType(animalDto.getAnimalType());
        if (file != null) {
            animal.setImage(file.getBytes());
        }
        return animalService.save(animal); // the save is also an update but you need to pass the id with the dto
        // without it it will create a new animal
    }

    @PutMapping(path = "")
    public int updateAnimal(@RequestBody AnimalDto animalDto) {
        Animal animal = new ModelMapper().map(animalDto, Animal.class);
        animalService.save(animal); // the save is also an update but you neet to pass the id with the dto
        return animal.getId();
    }

    @GetMapping(path = "/animalTypes")
    public List<AnimalType> fetchAllAnimalTypes() {
        return Arrays.asList(AnimalType.values());
    }


    @DeleteMapping("/{id}")
    public void removeById(@PathVariable("id") int id) throws NotFoundException {
        log.info("deleting animal with id {}", id);
        animalService.deleteById(id);
    }
}
