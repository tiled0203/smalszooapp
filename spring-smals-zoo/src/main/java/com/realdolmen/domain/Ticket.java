  package com.realdolmen.domain;

  import javax.persistence.Entity;
  import javax.persistence.OneToMany;
  import java.util.List;

  @Entity
  public class Ticket extends BaseEntity {

      private double originalPrice;
      private double reductionPrice;

      //Bidirectional because we have to know to which visitor a ticket belongs to
      @OneToMany(mappedBy = "ticket") //one ticket can belong to 1 or many visitors
      private List<Visitor> visitors;

      public Ticket() {
      }

      public Ticket(double originalPrice) {
          this.originalPrice = originalPrice;
      }



      public double getOriginalPrice() {
          return originalPrice;
      }

      public void setOriginalPrice(double price) {
          this.originalPrice = price;
      }

      public List<Visitor> getVisitors() {
          return visitors;
      }

      public void setVisitors(List<Visitor> visitors) {
          this.visitors = visitors;
      }

      public double getReductionPrice() {
          return reductionPrice;
      }

      public void setReductionPrice(double reductionPrice) {
          this.reductionPrice = reductionPrice;
      }
  }
