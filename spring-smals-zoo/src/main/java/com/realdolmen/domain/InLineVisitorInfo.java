package com.realdolmen.domain;

import org.springframework.beans.factory.annotation.Value;

//@Projection(name = "inlineInfo", types = {Visitor.class}) //SPRING-DATA-REST
public interface InLineVisitorInfo {
    VisitorPersonalInfoId getVisitorPersonalInfoId();

    @Value("#{target.getVisitorPersonalInfoId().getAddress()}")
    Address getAddress();
}
