@org.hibernate.annotations.NamedQueries({
        @org.hibernate.annotations.NamedQuery(name = "findAllAnimals", query = "select a from Animal a"),
        @org.hibernate.annotations.NamedQuery(name = "findAnimalByName", query = "select a from Animal a where a.name = :name")
})
package com.realdolmen.domain;
