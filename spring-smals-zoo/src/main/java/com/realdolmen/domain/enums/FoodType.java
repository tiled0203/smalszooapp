package com.realdolmen.domain.enums;

public enum FoodType {
    MEAT, VEGGIE, UNKNOWN, FISH, KIBBLE;
}
