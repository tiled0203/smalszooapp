package com.realdolmen.domain.enums;

public enum VisitorType {
    ADULT,CHILD,HANDICAP
}
