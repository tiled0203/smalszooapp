package com.realdolmen.domain.enums;

public enum AnimalType {
    BEAR("Bear" , FoodType.FISH),
    DOG("Dog", FoodType.MEAT),
    GIRAFFE("Giraffe", FoodType.VEGGIE),
    LION("Lion", FoodType.MEAT),
    DONKEY("Donkey", FoodType.VEGGIE),
    TIGER("Tiger", FoodType.MEAT),
    PIG("Pig", FoodType.VEGGIE),
    RABBIT("Rabbit", FoodType.VEGGIE),
    OWL("Owl", FoodType.MEAT),
    KANGAROO("Kangaroo", FoodType.VEGGIE);

    private final String name;
    private FoodType foodType;

    AnimalType(String name, FoodType foodType) {
        this.name = name;
        this.foodType = foodType;
    }

    public FoodType getFoodType() {
        return foodType;
    }

    public String getName() {
        return name;
    }

}
