package com.realdolmen.domain.enums;

public enum Visitor {
    ADULT,CHILD,HANDICAP
}
