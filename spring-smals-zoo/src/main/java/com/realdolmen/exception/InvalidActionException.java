package com.realdolmen.exception;

import javassist.NotFoundException;

public class InvalidActionException extends NotFoundException {
    public InvalidActionException(String s) {
        super(s);
    }
}
