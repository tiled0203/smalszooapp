package com.realdolmen.repositories.springdata;

import com.realdolmen.domain.Visitor;
import com.realdolmen.domain.VisitorPersonalInfoId;
import com.realdolmen.domain.enums.VisitorType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

//@RepositoryRestResource(excerptProjection = InLineVisitorInfo.class) //SPRING-DATA-REST
public interface VisitorRepository extends JpaRepository<Visitor, VisitorPersonalInfoId> {
    List<Visitor> findVisitorsByVisitorType(VisitorType visitorType);

    @Query("select distinct v.visitorType from Visitor v ")
    List<VisitorType> findDistinctVisitorTypes();

    Optional<Visitor> findVisitorsByVisitorPersonalInfoId(VisitorPersonalInfoId visitorPersonalInfoId);
}
