package com.realdolmen.repositories.springdata;

import com.realdolmen.domain.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Integer> {
}
