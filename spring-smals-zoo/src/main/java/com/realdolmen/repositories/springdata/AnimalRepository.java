package com.realdolmen.repositories.springdata;

import com.realdolmen.domain.Animal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
//@RepositoryRestResource(collectionResourceRel = "animal", path = "animal") //SPRING-DATA-REST
public interface AnimalRepository extends JpaRepository<Animal, Integer> {
    List<Animal> findByName(String name);
}
