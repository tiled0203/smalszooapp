package com.realdolmen.repositories.springdata;

import com.realdolmen.domain.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<AppUser, Long> {
    AppUser findAppUserByUserEmail(String email);
}
