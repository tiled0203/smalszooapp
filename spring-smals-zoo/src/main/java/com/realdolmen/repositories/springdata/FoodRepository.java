package com.realdolmen.repositories.springdata;

import com.realdolmen.domain.Food;
import com.realdolmen.domain.enums.AnimalType;
import com.realdolmen.domain.enums.FoodType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FoodRepository extends JpaRepository<Food, Integer> {
    List<Food> findFoodByAnimalType(@Param("animalType") AnimalType animalType);
    @Query("select distinct(f.foodType) from Food f")
    List<FoodType> findAllAvailableFoodTypes();
    @Query("select distinct(f.foodType) from Food f where f.animalType= :animalType")
    List<FoodType> findAllAvailableFoodTypesByAnimalType(AnimalType animalType);

}
