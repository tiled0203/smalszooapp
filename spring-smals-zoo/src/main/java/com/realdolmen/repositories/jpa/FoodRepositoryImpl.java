package com.realdolmen.repositories.jpa;

import com.realdolmen.domain.Animal;
import com.realdolmen.domain.Food;
import com.realdolmen.domain.enums.AnimalType;
import com.realdolmen.domain.enums.FoodType;
import com.realdolmen.repositories.jdbc.FoodRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@ConditionalOnProperty(name = "fetching.tech", havingValue = "jpa")
public class FoodRepositoryImpl implements FoodRepository {

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public void addFoodForAnimalType(Class<? extends Animal> animalType, Food food) {

    }

    @Override
    public List<Food> findFoodForAnimalType(Class<? extends Animal> animalClass) {
        return entityManager.createQuery("select f from Food f where f.animalType = :animalType and not f.foodType = 'KIBBLE'", Food.class)
                .setParameter("animalType", AnimalType.valueOf(animalClass.getSimpleName().toUpperCase())).getResultList();
    }

    @Override
    public List<Food> findFoodByFoodType(FoodType foodType) {
        return entityManager.createQuery("select f from Food f where f.foodType = :foodType", Food.class)
                .setParameter("foodType", foodType).getResultList();
    }
}
