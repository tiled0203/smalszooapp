package com.realdolmen.repositories.jdbc;

import com.realdolmen.domain.Animal;
import com.realdolmen.repositories.GenericCrudRepository;
import com.realdolmen.repositories.jdbc.mapper.AnimalMapper;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
@Qualifier("animal")
@ConditionalOnProperty(name = "fetching.tech", havingValue = "jdbc")
public class AnimalRepositoryImpl implements GenericCrudRepository<Animal> {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AnimalMapper animalMapper;

    @Autowired
    private DataSource dataSource;

    private List<Animal> animals = new ArrayList<>();
    @Override
    public List<Animal> findAll() {
        return jdbcTemplate.query("select * from Animal a left join  Food f on f.id = a.food_id ", animalMapper); // using a rowmapper
    }

    @Override
    public void save(Animal animal) { // TODO: still needs to be implemented with jdbc template
        animals.add(animal);
    }

    @Override
    public void delete(Animal animal) {// TODO: still needs to be implemented with jdbc template
        animals.remove(animal);
    }

    @Override
    public Animal findByName(String name) { // TODO: still needs to be implemented with jdbc template
        try {
            Optional<Animal> foundAnimal = animals.stream().filter(animal -> animal.getName().equals(name)).findFirst();
            return foundAnimal.orElseThrow(() -> new NotFoundException("Animal not found"));
        } catch (NotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
