package com.realdolmen.repositories.jdbc;

import com.realdolmen.annotations.IsKibble;
import com.realdolmen.domain.Animal;
import com.realdolmen.domain.Food;
import com.realdolmen.domain.Kibble;
import com.realdolmen.domain.enums.FoodType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@IsKibble
@Repository
@ConditionalOnProperty(name = "fetching.tech", havingValue = "jdbc")
public class KibbleFoodRepositoryImpl implements FoodRepository { // configured in zooconfig
//    private Map<Class<? extends Animal>, Kibble> foodMap = new HashMap<>(); // not needed anymore because we use the database

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Food mapRow(ResultSet rs, int rowNum) throws SQLException { // mapper for kibble food
        Kibble kibble = new Kibble();
        kibble.setFoodName(rs.getString("foodName"));
        kibble.setFoodType(FoodType.valueOf(rs.getString("foodType")));
        return kibble;
    }


    @Override
    public void addFoodForAnimalType(Class<? extends Animal> animalClass, Food food) {
//        foodMap.put(animalClass, (Kibble) food); // not needed anymore because we use the database with prefilled data
    }

    @Override
    public List<Food> findFoodForAnimalType(Class<? extends Animal> animalClass) {
        return jdbcTemplate.query("select * from Food where Food.foodType = 'KIBBLE' and Food.animalType = ?"
                , this::mapRow, animalClass.getSimpleName().toUpperCase());
    }

    @Override
    public List<Food> findFoodByFoodType(FoodType foodType) {
        return jdbcTemplate.query("select * from Food where Food.foodType = 'KIBBLE'", this::mapRow);
    }
}
