package com.realdolmen.repositories.jdbc;

import com.realdolmen.domain.Animal;
import com.realdolmen.domain.Food;
import com.realdolmen.domain.enums.FoodType;

import java.util.List;

public interface FoodRepository {
    void addFoodForAnimalType(Class<? extends Animal> animalType, Food food);

    List<Food> findFoodForAnimalType(Class<? extends Animal> animalClass);

    List<Food> findFoodByFoodType(FoodType foodType);
}
