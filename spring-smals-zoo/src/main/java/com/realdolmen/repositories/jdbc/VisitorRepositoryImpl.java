package com.realdolmen.repositories.jdbc;

import com.realdolmen.domain.Visitor;
import com.realdolmen.repositories.GenericCrudRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("visitor")
@ConditionalOnProperty(name = "fetching.tech", havingValue = "jdbc")
public class VisitorRepositoryImpl implements GenericCrudRepository<Visitor> {
    @Override
    public void save(Visitor entity) {

    }

    @Override
    public List<Visitor> findAll() {
        return null;
    }

    @Override
    public void delete(Visitor visitor) {

    }

    @Override
    public Visitor findByName(String name) {
        return null;
    }
}
