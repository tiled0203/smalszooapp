package com.realdolmen.repositories.jdbc;

import com.realdolmen.domain.Animal;
import com.realdolmen.domain.Food;
import com.realdolmen.domain.enums.FoodType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@ConditionalOnProperty(name = "fetching.tech", havingValue = "jdbc")
public class FoodRepositoryImpl implements FoodRepository { // bean is configured in zooconfig
    @Autowired
    private JdbcTemplate jdbcTemplate;
//    private Map<Class<? extends Animal>, Food> foodMap = new HashMap<>(); // not needed anymore because we use the database

    @Override
    public void addFoodForAnimalType(Class<? extends Animal> animalClass, Food food) { // not used anymore because we have prefilled data in DB
//        foodMap.put(animalClass, food);
    }

    @Override
    public List<Food> findFoodForAnimalType(Class<? extends Animal> animalClass) {
        return jdbcTemplate.query("select * from Food where animalType = ? and foodType != 'KIBBLE'", new BeanPropertyRowMapper<Food>(Food.class), animalClass.getSimpleName().toUpperCase());
    }

    @Override
    public List<Food> findFoodByFoodType(FoodType foodType) {
        return jdbcTemplate.query("select * from Food where foodType like :foodType", new BeanPropertyRowMapper<Food>(Food.class), foodType);
    }


}
