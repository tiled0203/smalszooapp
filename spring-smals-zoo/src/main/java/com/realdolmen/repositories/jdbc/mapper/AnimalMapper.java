package com.realdolmen.repositories.jdbc.mapper;

import com.realdolmen.domain.Animal;
import com.realdolmen.domain.Food;
import com.realdolmen.domain.enums.AnimalType;
import com.realdolmen.domain.enums.FoodType;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class AnimalMapper implements RowMapper<Animal> {

    @Override
    public Animal mapRow(ResultSet rs, int rowNum) throws SQLException {
        Animal animal = new Animal(rs.getString("name"), AnimalType.valueOf(rs.getString("animalType")), FoodType.valueOf(rs.getString("foodType")));

        if (rs.getInt("food_id") != 0) {
            Food food = new Food("special Kibble");
            food.setId(rs.getInt("food_id"));
            food.setFoodName(rs.getString("foodName"));
            animal.setFood(food);
            animal.setId(rs.getInt("id"));
        }
        return animal;
    }
}
