package com.realdolmen.configurations;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.time.LocalTime;

public class TimeCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        boolean condition = LocalTime.now().isAfter(LocalTime.of(20,0));
        System.out.println("Animals will get kibble: " + condition + " time is: " + LocalTime.now());
        return condition;
    }
}
