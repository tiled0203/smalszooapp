package com.realdolmen.configurations;

import com.realdolmen.domain.AppUser;
import com.realdolmen.domain.Authority;
import com.realdolmen.repositories.springdata.UserRepository;
import com.realdolmen.services.MyUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.sql.DataSource;
import java.util.Arrays;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {
    @Autowired
    private MyUserDetailService myUserDetailService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("admin").password(enc().encode("12345")).roles("ADMIN", "USER")
//                .and().withUser("user").password(enc().encode("12345")).roles("USER");

        auth.authenticationProvider(authProvider());

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,"/users","/login","/animals", "/animals/*", "/websocket").permitAll()
                .antMatchers(HttpMethod.DELETE, "/animals/*").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/animals").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and().formLogin().loginProcessingUrl("/login").and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout")).and()
                .csrf().disable().cors().disable().headers().frameOptions().disable(); //NOT FOR PRODUCTION
    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(myUserDetailService);
        authProvider.setPasswordEncoder(enc());
        return authProvider;
    }

//    @Override
//    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("/login").setViewName("login");
//    }

    @Bean
    PasswordEncoder enc() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public void createUser() {
        if (userRepository.count() <= 0) {
            AppUser user = new AppUser("Tom", "tom@email.com", enc().encode("12345"), Arrays.asList(new Authority("USER")));
            AppUser admin = new AppUser("admin", "admin@email.com", enc().encode("12345"), Arrays.asList(new Authority("ADMIN")));
            userRepository.saveAll(Arrays.asList(user, admin));
        }
    }

}
