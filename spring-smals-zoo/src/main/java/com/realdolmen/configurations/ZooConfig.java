package com.realdolmen.configurations;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import net.sf.ehcache.Cache;
import net.sf.ehcache.config.CacheConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.sql.DataSource;
import java.io.PrintStream;

@Configuration
@EnableAspectJAutoProxy
@EnableSwagger2
@EnableScheduling
@EnableCaching
@EnableAdminServer
@EnableJms
public class ZooConfig {


    @Bean
    public SimpleMailMessage templateSimpleMessage() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setText("This is the test email template for your email:<br><span style='color:blue'>%s</span>");
        return message;
    }

    @Bean
    public TaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(20);
        return scheduler;
    }

    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor scheduler = new ThreadPoolTaskExecutor();
        scheduler.setMaxPoolSize(20);
        return scheduler;
    }

    @Bean
    public EhCacheManagerFactoryBean cacheManager() {
        return new EhCacheManagerFactoryBean();
    }

    @Bean
    public EhCacheCacheManager testEhCacheManager() {
        // testEhCache Configuration - create configuration of cache that previous required XML
        CacheConfiguration testEhCacheConfig = new CacheConfiguration()
                .eternal(false)                     // if true, timeouts are ignored
                .timeToIdleSeconds(300)               // time since last accessed before item is marked for removal
                .timeToLiveSeconds(500)               // time since inserted before item is marked for removal
                .maxEntriesLocalHeap(50000)            // total items that can be stored in cache
                .memoryStoreEvictionPolicy("LRU")   // eviction policy for when items exceed cache. LRU = Least Recently Used
                .name("animalCache");

        Cache testCache = new Cache(testEhCacheConfig);

        cacheManager().getObject().addCache(testCache);
        return new EhCacheCacheManager(cacheManager().getObject());
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public PrintStream stream() {
        return System.out;
    }


    @Bean // 2nd way to configure datasource (easiest way is application.properties)
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setPassword("P@ssw0rd");
        dataSource.setUsername("root");
        dataSource.setUrl("jdbc:mysql://localhost:3306/zoo");
        return dataSource;
    }

}
