package com.realdolmen.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.realdolmen.domain.enums.AnimalType;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AnimalDto {
    private int id;

    @NotNull(message = "animalType may not be null!")
    private AnimalType animalType;
    private String name;
}
