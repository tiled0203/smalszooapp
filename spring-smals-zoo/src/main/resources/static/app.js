var stompClient = null;
$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { send(); });
    $( "#clear" ).click(function() { clear(); });
    setConnected(false);
});

function connect() {
    var socket = new SockJS('/websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/responses', function (response) {
            receive(JSON.parse(response.body));
        });
    });
}
function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function send() {
    stompClient.send("/app/request", {}, JSON.stringify({'author': $("#author").val(), 'message': $("#message").val() }));
}
function receive(message) {
    $("#messages").append("<tr><td>From: " + message.author + "</td><td>Message: " + message.message + "</td></tr>");
}
function clear() {
    $("#messages tr").remove();
}

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
        $("#name-form").show();
    }
    else {
        $("#conversation").hide();
        $("#name-form").hide();
    }
    $("#messages").html("");
}
