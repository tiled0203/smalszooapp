INSERT INTO Food(id, foodName, foodType, animalType)
VALUES (1, 'honey', 'VEGGIE', 'BEAR')
     , (2, 'salmon', 'FISH', 'BEAR')
     , (3, 'pedigree 77', 'MEAT', 'DOG')
     , (4, 'beef', 'MEAT', 'LION')
     , (5, 'leaf', 'VEGGIE', 'GIRAFFE')
     , (6, 'Giraffe leaf cookie', 'KIBBLE', 'GIRAFFE')
     , (7, 'Honey cookie', 'KIBBLE', 'BEAR')
     , (8, 'Bone', 'KIBBLE', 'DOG')
     , (9, 'beef', 'MEAT', 'DOG');

INSERT INTO Animal(id, animalType, foodType, name, food_id)
VALUES (1, 'DOG', 'MEAT', 'Fluffy', 3)
     , (2, 'GIRAFFE', 'VEGGIE', 'Tess', 6)
     , (3, 'BEAR', 'FISH', 'Blub', 2)
     , (4, 'LION', 'MEAT', 'Ugly', 4)
     , (5, 'GIRAFFE', 'VEGGIE', 'LongNeck', 6)
     , (6, 'DONKEY', 'VEGGIE', 'KingKong', 5)
     , (7, 'TIGER', 'MEAT', 'Stripes', NULL)
     , (8, 'PIG', 'VEGGIE', 'Miss Piggy', NULL)
     , (9, 'RABBIT', 'VEGGIE', 'Bugs Bunny', NULL)
     , (10, 'OWL', 'MEAT', 'Koekoek', NULL)
     , (11, 'KANGAROO', 'VEGGIE', 'Miloe', NULL)
     , (12, 'DOG', 'MEAT', 'Max', NULL)
     , (13, 'GIRAFFE', 'VEGGIE', 'Runaway', NULL)
     , (14, 'BEAR', 'VEGGIE', 'Baloe', NULL)
     , (15, 'GIRAFFE', 'VEGGIE', 'Yellow', NULL)
     , (16, 'DONKEY', 'VEGGIE', 'Luffy', NULL)
     , (17, 'TIGER', 'FISH', 'Jaws', NULL)
     , (18, 'PIG', 'VEGGIE', 'Big', NULL)
     , (19, 'RABBIT', 'VEGGIE', 'Lola Bunny', NULL)
     , (20, 'OWL', 'MEAT', 'Big Eye', NULL)
     , (21, 'KANGAROO', 'VEGGIE', 'Skippy', NULL)
     , (22, 'DOG', 'MEAT', 'Rolf', NULL)
     , (23, 'GIRAFFE', 'VEGGIE', 'Speedy', NULL)
     , (24, 'DONKEY', 'VEGGIE', 'Banana', NULL)
     , (25, 'TIGER', 'MEAT', 'Missy', NULL);

insert into Address(id, city, houseNumber, street)
values (1, 'Kalmthout', '4B', 'testStreet');

INSERT INTO Visitor(address_id, name, visitorType, ticket_fk, email)
VALUES (1, 'Tom', 'ADULT', NULL, 'tom@email.com'),
       (1, 'Jan', 'ADULT', NULL, 'jan@email.com'),
       (1, 'Liam', 'CHILD', NULL, 'liam@email.com');

