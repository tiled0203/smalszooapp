package com.realdolmen.restcontrollers;

import com.realdolmen.dto.AnimalDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class AnimalRestControllerTest {

    @Autowired
    private RestTemplate restTemplate ;

    @Test
    public void getAnimalTest(){
        ResponseEntity<AnimalDto[]> responseEntity = restTemplate.getForEntity("http://localhost:8080/animals", AnimalDto[].class);
        List<AnimalDto> animals = Arrays.asList(responseEntity.getBody());
        System.out.println(animals);
        Assertions.assertFalse(animals.isEmpty());
    }
}