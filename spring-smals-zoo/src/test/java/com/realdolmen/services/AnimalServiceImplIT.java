package com.realdolmen.services;

import com.realdolmen.BaseItTest;
import com.realdolmen.domain.Animal;
import com.realdolmen.exception.InvalidActionException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.junit.jupiter.params.shadow.com.univocity.parsers.conversions.Conversions.oneOf;


public class AnimalServiceImplIT extends BaseItTest { // integration test
    @Autowired
    @Qualifier("animal")
    private GenericCrudService<Animal> myAnimalService;
    @Autowired
    private FoodDistributionService foodDistributionService;

    @Test
    public void findAll() throws InvalidActionException {
        List<Animal> animals = myAnimalService.findAll();
        assertFalse(animals.isEmpty());
    }

    @Test
    public void testFindByNameAndAssertFood() throws InvalidActionException {
        foodDistributionService.feedAnimals(myAnimalService.findAll());
        Animal animal = myAnimalService.findByName("blub");
        assertNotNull(animal);
        assertNotNull(animal.getFood());
        assertThat(animal.getFood().getFoodName(), is(oneOf("salmon", "honey")));
    }


}
