package com.realdolmen.services;

import com.realdolmen.domain.Animal;
import com.realdolmen.domain.enums.AnimalType;
import com.realdolmen.domain.enums.FoodType;
import com.realdolmen.repositories.GenericCrudRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AnimalServiceImplTest {
    @Mock
    private GenericCrudRepository<Animal> animalGenericCrudRepository;

    @InjectMocks
    private AnimalServiceImpl animalMyService;

    @Before
    public void setUp() throws Exception {
        Animal bear = new Animal("TBaloe", AnimalType.BEAR, FoodType.VEGGIE);
        Animal lion = new Animal("TSimba", AnimalType.LION, FoodType.MEAT);
        Animal giraffe = new Animal("TLangePoot", AnimalType.GIRAFFE, FoodType.VEGGIE);
        when(animalGenericCrudRepository.findAll()).thenReturn(Arrays.asList(lion, bear, giraffe));
    }

    @Test
    public void findAll() {
        List<Animal> animals = animalMyService.findAll();
        Assert.assertEquals(3, animals.size());
        verify(animalGenericCrudRepository, times(1)).findAll();
    }

    @Test
    public void checkThatNameOfAnimalIsCorrect (){
        List<Animal> animals = animalMyService.findAll();
        Assert.assertTrue(animals.stream().anyMatch(animal -> animal.getName().equals("TLangePoot")));
    }


}
