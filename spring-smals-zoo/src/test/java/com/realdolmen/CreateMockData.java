package com.realdolmen;

import com.github.javafaker.Faker;
import com.realdolmen.domain.Animal;
import com.realdolmen.domain.enums.AnimalType;
import com.realdolmen.domain.enums.FoodType;
import com.realdolmen.repositories.springdata.AnimalRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@SpringBootTest(classes = MySpringApplication.class)
@ActiveProfiles("dev")
@RunWith(SpringRunner.class)
public class CreateMockData {
    @Autowired
    private AnimalRepository animalRepository;

    @Test
    public void createAnimals() {
        Faker faker = new Faker();
        Random random1 = new Random();
        Random random = new Random();
        List<AnimalType> animalTypes = Arrays.asList(AnimalType.values());
        List<FoodType> foodTypes = Arrays.asList(FoodType.values());

        int animalTypesCount = AnimalType.values().length;
        int foodTypesCount = FoodType.values().length;
        List<Animal> animalList = new ArrayList<>();
        for (int i = 0; i < 50000; i++) {
            AnimalType animalType = animalTypes.get(random1.nextInt(animalTypesCount));
            animalList.add(new Animal(faker.funnyName().name(), animalType, animalType.getFoodType()));
        }
        animalRepository.saveAll(animalList);

    }
}
