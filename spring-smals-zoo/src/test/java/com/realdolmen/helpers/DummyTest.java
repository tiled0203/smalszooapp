package com.realdolmen.helpers;

import com.realdolmen.BaseItTest;
import com.realdolmen.domain.Animal;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.List;


public class DummyTest extends BaseItTest {
    @Autowired
    private EntityManager entityManager;

    @Test
    public void test() {
        List<Animal> animals = entityManager.createQuery("select a from Animal a", Animal.class).getResultList();
        animals.forEach(animal -> System.out.println(animal.getName()));
    }
}
